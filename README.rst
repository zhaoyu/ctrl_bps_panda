##############
ctrl_bps_panda
##############

.. image:: https://img.shields.io/pypi/v/lsst-ctrl-bps-panda.svg
    :target: https://pypi.org/project/lsst-ctrl-bps-panda/
.. image:: https://codecov.io/gh/lsst/ctrl_bps_panda/branch/main/graph/badge.svg?token=YoPKBx96gw
 :target: https://codecov.io/gh/lsst/ctrl_bps_panda

``ctrl_bps_panda`` is a package in the `LSST Science Pipelines <https://pipelines.lsst.io>`_.

It provides a PanDA plugin for LSST PipelineTask execution framework, based on ``ctrl_bps``.

* SPIE paper from 2022: `The Vera C. Rubin Observatory Data Butler and Pipeline Execution System <https://arxiv.org/abs/2206.14941>`_.
* `User Guide <https://panda.lsst.io/>`_.


PyPI: `lsst-ctrl-mpexec <https://pypi.org/project/lsst-ctrl-bps-panda/>`_
